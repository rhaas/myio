/* 
    Author: Zeran Zhu
            National Center for Supercomputing Applications (NCSA), and
            Computer Engineering,
            University of Illinois at Urbana-Champaign (UIUC)
            zzhu35@ncsa.illinois.edu
            July 2018

    Mentor: Roland Haas
            NCSA,
            UIUC
            rhaas@ncsa.illinois.edu

hdf5_support.c:
    HDF5 file format support helper functions

*/

#include <stdlib.h>

#include "util_String.h"
#include "util_ErrorCodes.h"  

#include "hdf5_support.h"


void
h5append_attribute(hid_t loc, const char* name, const char* attrstring)
{
    if (!name || !attrstring) return;
    hsize_t i = 1;
    hid_t attr_sid = H5Screate_simple(1, &i, &i);
    hid_t vls_type_c_id = H5Tcopy(H5T_C_S1);
    H5CALL(H5Tset_size(vls_type_c_id, H5T_VARIABLE));
    hid_t attr = H5Acreate(loc, name, vls_type_c_id, attr_sid, H5P_DEFAULT, H5P_DEFAULT);
    H5CALL(H5Awrite(attr, vls_type_c_id, (const void*)&attrstring));
    H5CALL(H5Aclose(attr));
}

int
h5file_add_dset(hid_t fileid, h5dset_t* ptr, hsize_t hdf5_chunk_size)
{
    if (!ptr) return -1;
    hsize_t init[2];
    init[0] = 0;
    init[1] = ptr->cols;
    hsize_t unlimited[2];
    unlimited[0] = H5S_UNLIMITED;
    unlimited[1] = ptr->cols;
    hsize_t chunks[2];
    chunks[0] = hdf5_chunk_size;
    chunks[1] = ptr->cols;
    hid_t sid = H5Screate_simple(2, init, unlimited);
    hid_t creation_property_id = H5Pcreate(H5P_DATASET_CREATE);
    H5CALL(H5Pset_chunk(creation_property_id, 2, chunks));
    hid_t did = H5Dcreate(fileid, ptr->path, ptr->dtype, sid, H5P_DEFAULT, creation_property_id, H5P_DEFAULT);
    /* set description attribute */
    hid_t vls_type_c_id = H5Tcopy(H5T_C_S1);
    H5CALL(H5Tset_size(vls_type_c_id, H5T_VARIABLE));
    hsize_t attr_size = ptr->cols;
    hid_t attr_sid = H5Screate_simple(1, &attr_size, &attr_size);
    hid_t attr = H5Acreate(did, "description", vls_type_c_id, attr_sid, H5P_DEFAULT, H5P_DEFAULT);

    const char** desc = (const char**)malloc(attr_size*sizeof(char*));
    var_t* head = ptr->var_head;
    int i = attr_size-1;
    while(head && i > 0)
    {
        desc[i] = CCTK_FullVarName(head->idx);
        i--;
        head = head->next;
    }
    desc[0] = "cctk_time";
    H5CALL(H5Awrite(attr, vls_type_c_id, (const void*)desc));
    char* attr_string;
    if (ptr->output_every_time != -1)
    {
        Util_asprintf(&attr_string, "Initial output frequency: dt = %lf.\nFrequency might change in future appending. Refer to file attribute for possible changed frequency.", ptr->output_every_time);
        h5append_attribute(did, "frequency", attr_string);
        free(attr_string);
    }
    if (ptr->output_every_epoch != -1)
    {
        Util_asprintf(&attr_string, "Initial output frequency: every %d iteration(s).\nFrequency might change in future appending. Refer to file attribute for possible changed frequency", ptr->output_every_epoch);
        h5append_attribute(did, "frequency", attr_string);
        free(attr_string);
    }
    free(desc);
    H5CALL(H5Dclose(did));
    H5CALL(H5Pclose(creation_property_id));
    H5CALL(H5Sclose(sid));
    H5CALL(H5Sclose(attr_sid));
    H5CALL(H5Aclose(attr));
    return 0;
}

int
h5dset_append(h5dset_t* in, void** buf, hsize_t len)
{
    if (!in || !buf || len < 0)
    {
        return -1;
    }

    hid_t fileid = H5Fopen(in->file->fname, H5F_ACC_RDWR, H5P_DEFAULT);
    hid_t dsetid = H5Dopen(fileid, in->path, H5P_DEFAULT);

    hsize_t offset[2] = {in->len, 0};
    in->len += len;
    hsize_t dims[2] = {in->len, in->cols};
    H5CALL(H5Dset_extent(dsetid, dims));
    hsize_t ind_dims[2] = {len, 1};
    for (offset[1] = 0; offset[1] < in->cols; offset[1]++)
    {
        hid_t filespace = H5Dget_space(dsetid);
        H5CALL(H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL, ind_dims, NULL));
        hid_t memdataspace = H5Screate_simple(2, ind_dims, NULL);
        H5CALL(H5Dwrite(dsetid, in->dtype, memdataspace, filespace, H5P_DEFAULT, buf[offset[1]]));
        H5CALL(H5Sclose(filespace));
        H5CALL(H5Sclose(memdataspace));
    }
    H5CALL(H5Dclose(dsetid));
    H5CALL(H5Fclose(fileid));
    return CCTK_VarTypeSize(in->cctk_type)*(int)len*(int)in->cols;
}


// from PUGH IOHDF5
hid_t iozerod_IOHDF5Util_DataType(int cctk_type)
{
  hid_t retval;


  switch (cctk_type)
  {
    case CCTK_VARIABLE_BYTE:      retval = HDF5_BYTE; break;
    case CCTK_VARIABLE_INT:       retval = HDF5_INT; break;
    case CCTK_VARIABLE_REAL:      retval = HDF5_REAL; break;
    //case CCTK_VARIABLE_COMPLEX:   retval = myGH->HDF5_COMPLEX; break;
#ifdef HAVE_CCTK_INT1
    case CCTK_VARIABLE_INT1:      retval = HDF5_INT1; break;
#endif
#ifdef HAVE_CCTK_INT2
    case CCTK_VARIABLE_INT2:      retval = HDF5_INT2; break;
#endif
#ifdef HAVE_CCTK_INT4
    case CCTK_VARIABLE_INT4:      retval = HDF5_INT4; break;
#endif
#ifdef HAVE_CCTK_INT8
    case CCTK_VARIABLE_INT8:      retval = HDF5_INT8; break;
#endif
#ifdef HAVE_CCTK_REAL4
    case CCTK_VARIABLE_REAL4:     retval = HDF5_REAL4; break;
    //case CCTK_VARIABLE_COMPLEX8:  retval = myGH->HDF5_COMPLEX8; break;
#endif
#ifdef HAVE_CCTK_REAL8
    case CCTK_VARIABLE_REAL8:     retval = HDF5_REAL8; break;
    //case CCTK_VARIABLE_COMPLEX16: retval = myGH->HDF5_COMPLEX16; break;
#endif
#ifdef HAVE_CCTK_REAL16
    case CCTK_VARIABLE_REAL16:    retval = HDF5_REAL16; break;
    //case CCTK_VARIABLE_COMPLEX32: retval = myGH->HDF5_COMPLEX32; break;
#endif

    default: CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                         "Unsupported CCTK variable datatype %d", cctk_type);
             retval = -1;
  }

  return (retval);
}