/* 
    Author: Zeran Zhu
            National Center for Supercomputing Applications (NCSA), and
            Computer Engineering,
            University of Illinois at Urbana-Champaign (UIUC)
            zzhu35@ncsa.illinois.edu
            July 2018

    Mentor: Roland Haas
            NCSA,
            UIUC
            rhaas@ncsa.illinois.edu

IOZeroD.c:
    Implementation of thorn datastructure,
    helper functions and CCTK schedular functions

*/

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "util_String.h"
#include "util_ErrorCodes.h"  
#include "util_Table.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>


#include "IOZeroD.h"



// SUG: I would avoid defining the macros since they require people to learn
// something new, and really only save a very few lines of code
#define TRAVERSE_LL(head, stmt) do{ \
  while (head)                      \
  {                                 \
    stmt;                           \
    head = head->next;              \
  }                                 \
} while(0);

/* provides for loop style access to LL DS*/
#define FOR_ALL_LL(head, init, cond, evol, stmt) do{ \
  init;                                              \
  while (head && cond)                               \
  {                                                  \
    stmt;                                            \
    head = head->next;                               \
    evol;                                            \
  }                                                  \
} while(0);



/* global place holder */
static iozero_t* iozero = NULL;

/* parse arguments and set up datastructure according to parameter string */
static void thorn_setup();
/* clean up runtime data */
static void thorn_clearup();
/* linked list free helper function */
static void free_dset_ll(h5dset_t* in);
static void free_var_ll(var_t* in);
/* argument parser callback function */
static void parse_hdf5_args(int idx, const char *optstring, void *callback_arg);
/* allocates and initializes a var_t type */
static var_t* new_var(int idx, int type, var_t* next);
/* checks if there is data in buffer, flush to disk if true */
static void buf_evict_all(void);
/* flush a specific data set to disk */
static void buf_evict(h5dset_t* in);



int
IOZeroD_OutputGH(const cGH *cctkGH)
{
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;
  if (CCTK_MyProc(cctkGH)) return 0;
  /* check whether the output var parameter string is changed or not */
  if (strcmp(iozero->var_string, h5_output_var_name))
  {
    thorn_clearup();
    thorn_setup();
  }

  h5dset_t* dset = iozero->h5dset_head;
  while (dset)
  {
    /* determine whether should output */
    if (dset->output_every_epoch > 0)
    {
      if (dset->last_touched_epoch+dset->output_every_epoch > cctk_iteration)
      {
        dset = dset->next;
        continue;
      }
    }
    if (dset->output_every_time > 0)
    {
      CCTK_REAL curr = cctk_time;
      if (dset->last_touched_time+dset->output_every_time > curr)
      {
        dset = dset->next;
        continue;
      }
    }
    
    var_t* var = dset->var_head;
    size_t var_size = CCTK_VarTypeSize(dset->cctk_type);
    /* if buffer is about to overflow, evict */
    if ((dset->occupied+1) * var_size > mem_cache_size_per_var)
      buf_evict(dset);
    
    TRAVERSE_LL(var,
      void* cur_val = CCTK_PTR_FROM_IDX(var->idx);
      void* dst = var->buf + dset->occupied * var_size;
      memcpy(dst, cur_val, var_size);
    )

    void* dst = dset->tbuf + dset->occupied++ * var_size;
    /* cast time according to dset type */
    CCTK_CHAR tvar1; CCTK_INT tvar2; CCTK_REAL tvar3;
    switch(dset->cctk_type)
    {
      case CCTK_VARIABLE_CHAR:
        tvar1 = (CCTK_CHAR)cctk_time;
        memcpy(dst, (void*)&tvar1, var_size);
        break;
      case CCTK_VARIABLE_INT:
        tvar2 = (CCTK_INT)cctk_time;
        memcpy(dst, (void*)&tvar2, var_size);
        break;
      case CCTK_VARIABLE_REAL:
        tvar3 = (CCTK_REAL)cctk_time;
        memcpy(dst, (void*)&tvar3, var_size);
        break;
      default:
        CCTK_VERROR("Unsopported file type %d", dset->cctk_type);
    }
    dset->last_touched_time = cctk_time;
    dset->last_touched_epoch = cctk_iteration;

    dset = dset->next;
  }
  return 0;
}

/* find the variable's existence in all possible datasets */
int
IOZeroD_TriggerOutput(const cGH *cctkGH, int vindex)
{
  if (CCTK_MyProc(cctkGH)) return 0;
  return 0;
}

int
IOZeroD_TimeToOutput(const cGH *cctkGH, int vindex)
{
  if (CCTK_MyProc(cctkGH)) return 0;
  return 0;
}

int
IOZeroD_OutputVarAs(const cGH *cctkGH, const char *var, const char *alias)
{
  DECLARE_CCTK_PARAMETERS;
  if (CCTK_MyProc(cctkGH)) return 0;
  int idx = CCTK_VarIndex(var);
  h5dset_t* new_dset = (h5dset_t*)malloc(sizeof(h5dset_t));
  new_dset->next = NULL;
  char* path;
  Util_asprintf(&path, "/%s", alias);
  new_dset->path = path;
  new_dset->file = iozero->h5file;
  new_dset->cols = 2; // time + the new var
  new_dset->len = 0;
  new_dset->dtype = HDF5_TYPE_FROM_IDX(idx);
  new_dset->cctk_type = CCTK_TYPE_FROM_IDX(idx);
  if (CCTK_VarTypeSize(new_dset->cctk_type) > mem_cache_size_per_var)
    CCTK_VERROR("Minimum buffer size requirement not met. Dataset \"%s\" needs %d but maximum is only %d.", new_dset->path, CCTK_VarTypeSize(new_dset->cctk_type), mem_cache_size_per_var);
  new_dset->tbuf = malloc(iozero->max_buf_size);
  if (!new_dset->tbuf)
    CCTK_VERROR("Malloc failed on request of size %lu", iozero->max_buf_size);
  new_dset->var_head = new_var(idx, 1, NULL);
  new_dset->last_touched_epoch = 0;
  new_dset->last_touched_time = 0.0f;
  new_dset->output_every_time = -1.0f;
  new_dset->output_every_epoch = 1;
  new_dset->occupied = 0;
  new_dset->next = iozero->h5dset_head;
  iozero->h5dset_head = new_dset;

  hid_t fileid = H5Fopen(new_dset->file->fname, H5F_ACC_RDWR, H5P_DEFAULT);
  CCTK_ASSERT(fileid > 0, "Unable to open file.");
  if (h5file_add_dset(fileid, new_dset, hdf5_chunk_size))
  {
    CCTK_VWARN(1, "Error adding dataset '%s' to file", new_dset->path);
  }
  return 0;
}





int
IOZeroD_Startup(void)
{
  int handle = CCTK_RegisterIOMethod("IOZeroDIO");

  if (handle < 0)
  {
    CCTK_WARN(1, "Error registering IO routines, there will be no time series output from IOZeroD.");
  }
  CCTK_RegisterIOMethodOutputGH(handle, IOZeroD_OutputGH);
  CCTK_RegisterIOMethodTimeToOutput(handle, IOZeroD_TimeToOutput);
  CCTK_RegisterIOMethodTriggerOutput(handle, IOZeroD_TriggerOutput);
  CCTK_RegisterIOMethodOutputVarAs(handle, IOZeroD_OutputVarAs);
  CCTK_INFO("Registered IO routines.");
  return 0;
}

static int init = 0;
void
IOZeroD_Init(CCTK_ARGUMENTS)
{
  if (CCTK_MyProc(cctkGH)) return;
  if (init) return;
  init = 1;
  thorn_setup();
  CCTK_INFO("Successfully initialized thorn.");

}
void
IOZeroD_Cleanup(CCTK_ARGUMENTS)
{
  if (CCTK_MyProc(cctkGH)) return;
  thorn_clearup();
  CCTK_INFO("Cleaned up thorn.");
}

void
IOZeroD_Checkpoint(CCTK_ARGUMENTS)
{
  if (CCTK_MyProc(cctkGH)) return;
  buf_evict_all();
}

void
thorn_setup()
{
  DECLARE_CCTK_PARAMETERS;
  iozero = (iozero_t*)malloc(sizeof(iozero_t));
  if (!iozero)
  {
    CCTK_ERROR("Unable to allocate memory for internal usage.");
  }
  iozero->max_buf_size = mem_cache_size_per_var;
  iozero->h5dset_head = NULL;
  iozero->h5file = (h5file_t*)malloc(sizeof(h5file_t));
  iozero->h5file->fname = NULL;
  const char* out_dir = cctk_iozerod_outdir;
  if (!strcmp("", cctk_iozerod_outdir))
    out_dir = io_out_dir;
  Util_asprintf(&(iozero->h5file->fname), "%s/%s_%d.hdf5", out_dir, CCTK_THORNSTRING, CCTK_MyProc(NULL));
  /* store variable string */
  Util_asprintf(&(iozero->var_string), "%s", h5_output_var_name);

  hid_t fileid; int append = 0;
  /* if file writable, append */
  if (access(iozero->h5file->fname, W_OK) != -1)
  {
    fileid = H5Fopen(iozero->h5file->fname, H5F_ACC_RDWR, H5P_DEFAULT);
    CCTK_ASSERT(fileid > 0, "Unable to open file.");
    char* attr_string;
    time_t current_time = time(NULL);
    Util_asprintf(&attr_string, "File opened for simulation output.\nOutput variables: %s\n\n%s",h5_output_var_name, ctime(&current_time));
    h5append_attribute(fileid, ctime(&current_time), attr_string);
    free(attr_string);
    append = 1;
  }
  /* if file exists but not writable*/
  else if (access(iozero->h5file->fname, F_OK) != -1)
  {
    CCTK_VERROR("Failed to access file %s, write operation denied.", iozero->h5file->fname);
    return;
  }
  /* if file does not exist, create new one */
  else
  {
    /* ensures that the directory exists */
    (void)CCTK_CreateDirectory(0755, out_dir);
    fileid = H5Fcreate(iozero->h5file->fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    CCTK_ASSERT(fileid > 0, "Unable to create file.");
    /* write file description */
    char* attr_string;
    time_t current_time = time(NULL);
    Util_asprintf(&attr_string, "HDF5 simulation output file.\nGenerated by IOZeroD thorn.\n\n%s", ctime(&current_time));
    h5append_attribute(fileid, "created", attr_string);
    free(attr_string);
    Util_asprintf(&attr_string, "File opened for simulation output.\nOutput variables: %s\n\n%s",h5_output_var_name, ctime(&current_time));
    h5append_attribute(fileid, ctime(&current_time), attr_string);
    free(attr_string);
  }


  CCTK_TraverseString(h5_output_var_name, parse_hdf5_args, NULL, CCTK_GROUP_OR_VAR);
  

  h5dset_t* ptr = iozero->h5dset_head;
  hid_t did;
  while(ptr)
  {
    /* add new dataset */
    if (!append)
    {
      if (h5file_add_dset(fileid, ptr, hdf5_chunk_size))
      {
        CCTK_VWARN(1, "Error adding dataset '%s' to file", ptr->path);
      }
    }

    /* check if the dataset exists */
    H5O_info_t info;
    info.type = H5O_TYPE_UNKNOWN;
    H5CALL(H5Oget_info_by_name(fileid, ptr->path, &info, H5P_DEFAULT));
    if (info.type == H5O_TYPE_DATASET)
    {
      did = H5Dopen(fileid, ptr->path, H5P_DEFAULT);
      CCTK_ASSERT(did > 0, "Unable to open dataset.");
      hid_t sid = H5Dget_space(did);
      CCTK_ASSERT(sid > 0, "Unable to get dataspace.");
      hsize_t size[2];
      hsize_t max_size[2];
      H5CALL(H5Sget_simple_extent_dims(sid, size, max_size));
      CCTK_ASSERT(size[1]==ptr->cols, "Assertion failed: dataset not compatible.");
      ptr->len = size[0];
      H5CALL(H5Dclose(did));
      H5CALL(H5Sclose(sid));
    }
    else
    {
      CCTK_VERROR("Name of dset: \"%s\" occupied", ptr->path);
    }



    char* typename = NULL;
    switch(ptr->cctk_type)
    {
      case CCTK_VARIABLE_CHAR:
        typename = "CCTK_CHAR";
        break;
      case CCTK_VARIABLE_INT:
        typename = "CCTK_INT";
        break;
      case CCTK_VARIABLE_REAL:
        typename = "CCTK_REAL";
        break;
      default:
        typename = "OTHER";
    }
    CCTK_VINFO("Successfully setup DATASET: \"%s\"; TYPE: %s; FREQ: %lf & %d it", ptr->path, typename, ptr->output_every_time, ptr->output_every_epoch);

    ptr = ptr->next;
  }
  H5CALL(H5Fclose(fileid));
}

/* clean u helper function */
void
thorn_clearup()
{
  /* write to file if leftover */
  if (!iozero) return;
  buf_evict_all();
  free_dset_ll(iozero->h5dset_head);
  free(iozero->h5file->fname);
  free(iozero->var_string);
  free(iozero->h5file);
  free(iozero);
}

void
free_dset_ll(h5dset_t* in)
{
  if (!in) return;
  free_dset_ll(in->next);
  free_var_ll(in->var_head);
  free(in->path);
  free(in->tbuf);
  free(in);
}

void
free_var_ll(var_t* in)
{
  if (!in) return;
  free_var_ll(in->next);
  free(in->buf);
  free(in);
}


void
parse_hdf5_args(int idx, const char *optstring, void *callback_arg)
{
  DECLARE_CCTK_PARAMETERS;
  if (idx < 0) return;
  if (CCTK_GroupTypeFromVarI(idx) == CCTK_GF)
  {
    CCTK_VWARN(1, "Variable %s is of type GF, ignoring its output.", CCTK_FullVarName(idx));
    return;
  }
  h5dset_t* ptr = iozero->h5dset_head;

  const char* dset_name;
  CCTK_REAL output_every_time;
  int output_every_epoch; 

  if (!optstring) optstring = "";
  /* default output property is full variable name and output every epoch */
  if (optstring[0] == '\0')
  {
    dset_name = CCTK_FullVarName(idx);
    output_every_time = -1.0f;
    output_every_epoch = 1;
  }
  else
  {
    int handle = Util_TableCreateFromString(optstring);
    char buf[MAX_DSET_NAME];
    dset_name = buf;
    int dset_name_len = Util_TableGetString(handle, MAX_DSET_NAME, buf, "dataset");
    /* set output frequency */
    int res = Util_TableGetReal(handle, &output_every_time, "out_dt");
    if (res < 0) output_every_time = -1.0f;
    res = Util_TableGetInt(handle, &output_every_epoch, "out_it");
    if (res < 0) output_every_epoch = -1;
    if (output_every_time < 0 && output_every_epoch < 0)
    {
      output_every_epoch = 1;
      CCTK_VWARN(1, "Invalid configuration: \"%s\" neither output frequency is set. Setting output frequency to per epoch/iteration.", dset_name);
    }
    /* if no dataset name */
    if (dset_name_len <= 0)
    {
      dset_name = CCTK_FullVarName(idx);
    }
    else
    {
      TRAVERSE_LL(ptr,
      /* check if dataset exists, path[0] is '/' */ 
        if (!strcmp(ptr->path+1, dset_name))
        {
          /* allocate new var object in dataset */
          CCTK_ASSERT(HDF5_TYPE_FROM_IDX(idx)==ptr->dtype, "Assertion failed: variables in a dset should have the same type.");
          CCTK_ASSERT(output_every_time==ptr->output_every_time && output_every_epoch==ptr->output_every_epoch, "Assertion failed: inconsistent output frequency found in a dataset.");
          ptr->var_head = new_var(idx, 1, ptr->var_head);
          ptr->cols++;
          return;
        }
      )
    }

  }
 
  
  /* not able to find, insert at front */
  h5dset_t* head = (h5dset_t*)malloc(sizeof(h5dset_t));
  head->next = NULL;
  /* prepare dataset path, set first char to '/' */
  char* path;
  Util_asprintf(&path, "/%s", dset_name);
  head->path = path;
  head->file = iozero->h5file;
  head->cols = 2; // time + the new var
  head->len = 0;
  head->dtype = HDF5_TYPE_FROM_IDX(idx);
  head->cctk_type = CCTK_TYPE_FROM_IDX(idx);
  if (CCTK_VarTypeSize(head->cctk_type) > mem_cache_size_per_var)
    CCTK_VERROR("Minimum buffer size requirement not met. Dataset \"%s\" needs %d but maximum is only %d.", head->path, CCTK_VarTypeSize(head->cctk_type), mem_cache_size_per_var);
  head->tbuf = malloc(iozero->max_buf_size);
  if (!head->tbuf)
    CCTK_VERROR("Malloc failed on request of size %lu", iozero->max_buf_size);
  head->var_head = new_var(idx, 1, NULL);
  head->last_touched_epoch = 0;
  head->last_touched_time = 0.0f;
  head->output_every_time = output_every_time;
  head->output_every_epoch = output_every_epoch;
  head->occupied = 0;
  head->next = iozero->h5dset_head;
  iozero->h5dset_head = head;

}


// constructs a new variable struct
var_t*
new_var(int idx, int type, var_t* next)
{
  var_t* head = (var_t*)malloc(sizeof(var_t));
  head->type = type;
  head->idx = idx;
  head->buf = malloc(iozero->max_buf_size);
  if (!head->buf) CCTK_ERROR("malloc failed");
  head->next = next;
  return head;
}

void
buf_evict_all(void)
{
  if (!iozero) return;
  h5dset_t* dset = iozero->h5dset_head;
  TRAVERSE_LL(dset,
    buf_evict(dset);
  )
}

void
buf_evict(h5dset_t* dset)
{
  if (dset->occupied)
    {
      void** all_buf = (void**)malloc(dset->cols*sizeof(void*));
      /* time goes first */
      all_buf[0] = dset->tbuf;
      var_t* var = dset->var_head;
      /* linked list is reversed because of head insertion */
      FOR_ALL_LL(var, int i = dset->cols-1, i > 0, i--,
        all_buf[i] = var->buf;
      )
      /* write to dataset */
      int bufsize = h5dset_append(dset, all_buf, dset->occupied);
      CCTK_VINFO("Written %d bytes in \"%s\" with %lu columns", bufsize, dset->path, (size_t)dset->cols);
      dset->occupied = 0;
  }
}