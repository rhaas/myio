/* 
    Author: Zeran Zhu
            National Center for Supercomputing Applications (NCSA), and
            Computer Engineering,
            University of Illinois at Urbana-Champaign (UIUC)
            zzhu35@ncsa.illinois.edu
            July 2018

    Mentor: Roland Haas
            NCSA,
            UIUC
            rhaas@ncsa.illinois.edu

IOZeroD.h:
    Declaration of thorn datastructure,
    helper functions and CCTK schedular functions

*/

#ifndef _IOZEROD_H_
#define _IOZEROD_H_

#include "cctk.h"
#include "cctk_Arguments.h"

#include <stdio.h>

#include "hdf5_support.h"


#define OUT_ASCII_NAME ("zerod.txt")
#define OUT_HDF5_NAME ("zerod.hdf5")


#define CCTK_ASSERT(stmt, msg) do{ \
  int result = stmt;          \
  if (!result) CCTK_ERROR(#stmt " " msg); \
} while(0);

#define CCTK_CHECK(stmt, msg) do{ \
  int result = stmt;          \
  if (!result) CCTK_WARN(#stmt " " msg); \
} while(0);

typedef struct iozero_t
{
  FILE* outfile;
  size_t max_buf_size;
  h5dset_t* h5dset_head;
  h5file_t* h5file;
  char* var_string;
} iozero_t;


int IOZeroD_Startup(void);
void IOZeroD_Init(CCTK_ARGUMENTS);
void IOZeroD_Cleanup(CCTK_ARGUMENTS);
void IOZeroD_Checkpoint(CCTK_ARGUMENTS);

int IOZeroD_OutputGH(const cGH *cctkGH);
int IOZeroD_TriggerOutput(const cGH *cctkGH, int vindex);
int IOZeroD_TimeToOutput(const cGH *cctkGH, int vindex);
int IOZeroD_OutputVarAs(const cGH *cctkGH, const char *var, const char *alias);




#endif


