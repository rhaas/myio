What I'm trying to do is the following:
1. Initialize a target integer at STARTUP
2. Register SetupGH() ( This part is now commented out in @TODO )
3. IO functions
4. Try using rand() to continuously hit a range of integers
5. Exit simulation if target is hit by rand()

The code in the repo is working, but it will not run normally when you uncomment the @TODO part.
That's why I'm still using the "static variable hack" because I could not get GHExtension to work properly.

Ignore the Multiprocessor stuff for now, I'm just playing around with it and it seems not working on my laptop as well.
The reason might be that I'm using the static variables.


6/29/2018
There are still memory leak detected by valgrind even if I free my GHExt
How to make all processors get the same target?
IOZeroD scratch. Can output WaveMol::energy

7/12/2018
Finished HDF5 times series output with multi var and dset

## TODO:
 - [x] ASCII output support (not needed)
 - [x] Output every *
 - [x] parse option list
 - [x] frequency parsing
 - [x] TimeToOutput (not needed)
 - [x] checkpointing and cacheNflushing
 - [x] error checking
 - [ ] cleanup and documentation
 - [x] use IOUtil dir as default output dir
 - [x] add hdf5 dataset appending functionality

## Regarding the TimeToOutput:
 - [x] many vars of same index may appear in different datasets. multiple solutions
## Regarding appending dataset:
 - [x] need to suppress warning about searching for dataset name


Code Complete